#include <Arduino.h>
#include <memory>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "debugLogger.h"
#include "mqtt.h"
#include "configHelpers.h"

#ifndef MQTT_GLOBALS
#define MQTT_GLOBALS

// TODO Move this into a config header/source
#define MQTT_SERVER     "192.168.178.2"
#define MQTT_PORT       1884UL
#define MQTT_USER       "olaf_mqtt"
#define MQTT_PASSWORD   "_lfK5G3phaLD"

extern DeviceName devName;
extern DebugLogger logger;
WiFiClient espClient;
PubSubClient mqttClient(espClient);
SubscribedMqttTopicList* subscribedTopicsList = NULL;
void (*savedSubscriptionCallback)();
bool setupDone = false;
extern bool wifiIsConnected;

#endif

SubscribedMqttTopic::SubscribedMqttTopic(
        char* fullTopicStr, 
        void (*callbackFunction)(char*))
{
    topic = fullTopicStr;
    callback = callbackFunction;
}

SubscribedMqttTopicList::SubscribedMqttTopicList(
        SubscribedMqttTopic* newTopic, 
        SubscribedMqttTopicList* nextElement)
{
    entry = newTopic;
    next = nextElement;
}

char* SubscribedMqttTopic::getTopic()
{
    return topic;
}

char* SubscribedMqttTopicList::getTopic()
{
    return entry->getTopic();
}

SubscribedMqttTopic* SubscribedMqttTopicList::getEntry()
{
    return entry;
}

SubscribedMqttTopicList* SubscribedMqttTopicList::getNext()
{
    return next;
}

void onMqttMessage(char* topic, byte* payload, unsigned int length)
{
    char* plStr = (char*) calloc(length+1, sizeof(char));
    strncpy(plStr, (char*) payload, length);
    *(plStr + length) = 0;
    logger.printf("MQTT: %s (len: %d) - Payload: %s\n", topic, length, plStr);
    SubscribedMqttTopicList* stl = subscribedTopicsList;
    while(stl != NULL)
    {
        if (strcmp(stl->getTopic(), topic) == 0)
        {
            logger.printf("Found topic: %s\n", stl->getTopic());
            stl->getEntry()->callback(plStr);
            break;
        }
        stl = stl->getNext();
    }
    logger.printf("-----------------\n");
    free(plStr);
}

void subscribeToTopic(const char* topic, void (*callback)(char*))
{
    if (!wifiIsConnected || !mqttClient.connected()) return;
    mqttClient.subscribe(topic);
    SubscribedMqttTopic* newTopic = new SubscribedMqttTopic(const_cast<char*>(topic), callback);
    SubscribedMqttTopicList* newTopicListElem = new SubscribedMqttTopicList(newTopic, subscribedTopicsList);
    subscribedTopicsList = newTopicListElem;
    logger.printf("Subscribed to MQTT-topic: %s\n", topic);
}

void connectToMqtt(void (*subscriptionCallback)())
{
    boolean successfullyConnected = false;
    while (!mqttClient.connected())
    {
        logger.printf("Attempting MQTT connection... ");
        logger.printf("\nMQTT: Using credentials: %s PW: %s\n", 
                        MQTT_USER, MQTT_PASSWORD);
        successfullyConnected = mqttClient.connect(devName.get(), MQTT_USER, MQTT_PASSWORD);
        


        if (successfullyConnected)
        {
            logger.printf("MQTT connected!\n");
            subscriptionCallback();
        }
        else
        {
            logger.printf("MQTT connection failed, rc=%d \ntry again in 1 second.\n", mqttClient.state());
            // Wait at least 1 second before retrying
            iotWebConfDelay(999);
            return;
        }
    }
}

void mqttSetup(void (*subscriptionCallback)())
{
    savedSubscriptionCallback = subscriptionCallback;
    if (!setupDone)
    {
        Serial.printf("mqttSetup: %s:%lu\n", MQTT_SERVER, MQTT_PORT);
        mqttClient.setServer(MQTT_SERVER, MQTT_PORT);
        mqttClient.setCallback(onMqttMessage);
        setupDone = true;
    }
}

void publishToMqtt(const char* topic, char* payload)
{
    if (!wifiIsConnected || !mqttClient.connected()) return;
    mqttClient.publish(topic, payload);
}

bool mqttConnected()
{
    return mqttClient.connected();
}

void mqttLoop()
{
    if (!setupDone) return;
    if (wifiIsConnected && !mqttClient.connected())
    {
        connectToMqtt(savedSubscriptionCallback);
    }
    mqttClient.loop();
}