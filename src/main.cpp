#include <Arduino.h>
#include <IotWebConf.h>
#include <SoftwareSerial.h>
#include "debugLogger.h"
#include "configHelpers.h"
#include "mqtt.h"
#include "renogyBattery.h"


#define MEASUREMENT_CYCLE_MS 1000
#define MQTT_PUBLISH_CYCLE_MS 1000

#define MQTT_BATTERY_TOPIC  "sensors/battery"
#define MQTT_SOLAR_TOPIC    "sensors/solar_power"

#define ENABLE_SOLAR_MONITOR    (0)

#define CONFIG_VERSION "v0.2.1"

// Global variables
DebugLogger logger;
DeviceName devName;
WebServer server(80);
const char wifiInitialApPassword[] = "SkiPrint2Room=";
DNSServer dnsServer;
IotWebConf iotWebConf(devName.get(), &dnsServer, &server, wifiInitialApPassword, CONFIG_VERSION);

// Local variables
SoftwareSerial rs485_serial(D6, D5); // RX, TX
static RenogyBattery battery(100.0f, rs485_serial, MEASUREMENT_CYCLE_MS);

// Cyclic functions
static unsigned long lastPublish = 0;


void setup()
{
    // TODO Set powerhold to open drain
    //pinMode(8, OUTPUT_OPEN_DRAIN);
    Serial.begin(115200);
    rs485_serial.begin(9600);

    while((!Serial) && (!rs485_serial)) {}  // Wait for Serial to start
    delay(750);
    devName.print();
    initWifiAP();
    battery.begin();
    Serial.println("Setup complete.");
}

void publish()
{
    // Battery stats.
    char battery_json[130];
    sprintf(battery_json, "{\"voltage\": %2.2f, \"current\": %2.2f, \"temperature\": %2.2f, \"soc\": %2.2f, \"soh\": %2.2f, \"remaining charge\": %2.2f}", battery.getVoltage(), battery.getCurrent(), battery.getTemperature(), battery.getSoc(), battery.getSoh(), battery.getRemainingCharge());
    publishToMqtt(MQTT_BATTERY_TOPIC, battery_json);
}

void loop()
{
    unsigned long now = millis();
    iotWebConf.doLoop();
    loopWifiChecks();

    battery.handle_communication();

    if((now - lastPublish) > MQTT_PUBLISH_CYCLE_MS)
    {
        publish();
        lastPublish = now;
    }

    logger.printAllWithSerial();
}