#pragma once
#ifndef DEV_NAME_CLASS
#define DEV_NAME_CLASS

class DeviceName {
    public:
        DeviceName();
        void print();
        char* get();

    private:
        char deviceName[23] = {'B', 'A', 'T', 'M', 'O', 'N', 'R', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
};

#endif

void initWifiAP();
char* getMqttServer();
unsigned long getMqttPort();
char* getMqttUsername();
char* getMqttPassword();
void loopWifiChecks();
void iotWebConfDelay(unsigned long duration);