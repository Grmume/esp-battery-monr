#include "renogyBattery.h"
#include <Arduino.h>

//extern DebugLogger logger;

RenogyBattery::RenogyBattery(float nominal_capacity_Ah, Stream& serial, uint16_t readout_cycletime):
                                _cycletime(readout_cycletime),
                                _nominal_capacity(static_cast<charge_t>(nominal_capacity_Ah*1000.0f)),
                                _serial(serial)
{
}

void RenogyBattery::begin(uint8_t slave_address)
 {
    _slave_address = slave_address;
    _comState = ComState::READ_TEMPS;
    _node.begin(_slave_address, _serial);
 }

void RenogyBattery::handle_communication()
{
    // Read the required register data.
    // See https://github.com/Grmume/renogy-smart-battery for details about the available registers

    long now = millis();
    if(now - _lastTransmissionMillis > RENOGY_BATTERY_TRANSMISSION_DELAY_MILLIS)
    {
        _lastTransmissionMillis = now;
        // Trigger next transmission
        switch(_comState)
        {
            case ComState::READ_TEMPS:
            {
                // Check if the next readout cycle should be started:
                if(now - _startOfReadoutCycleMillis > _cycletime)
                {
                    _startOfReadoutCycleMillis = now;
                    // Cell temperatures
                    uint8_t result = _node.readHoldingRegisters(0x139aU, 4);
                    if (result == ModbusMaster::ku8MBSuccess)
                    {
                        _cell_temps[0] = _node.getResponseBuffer(0);
                        _cell_temps[1] = _node.getResponseBuffer(1);
                        _cell_temps[2] = _node.getResponseBuffer(2);
                        _cell_temps[3] = _node.getResponseBuffer(3);

                        char temps_string[40];
                        sprintf(&temps_string[0], "Cell temps: %u,%u,%u,%u\n", _cell_temps[0], _cell_temps[1], _cell_temps[2], _cell_temps[3]);
                        Serial.print(temps_string);
                    }
                    else
                    {
                        char error_string[15];
                        sprintf(&error_string[0], "Error:%u\n", result);
                        Serial.print(error_string);
                    }
                    _comState = ComState::READ_BATTERYINFO;
                }
                else
                {
                    // Stay in state READ_TEMPS until new cycle should be started.
                }
                
                break;
            }
            case ComState::READ_BATTERYINFO:
            {
                // Battery status
                uint8_t result = _node.readHoldingRegisters(0x13b2U, 6);
                if (result == ModbusMaster::ku8MBSuccess)
                {
                    _current = static_cast<current_t>(_node.getResponseBuffer(0));
                    _battery_voltage = static_cast<voltage_t>(_node.getResponseBuffer(1));

                    _remaining_charge = (_node.getResponseBuffer(2) << 16) |
                                        _node.getResponseBuffer(3);

                    _capacity = (_node.getResponseBuffer(4) << 16) |
                                _node.getResponseBuffer(5);

                    char battery_info[90];
                    sprintf(&battery_info[0], "Current: %u, Voltage: %u, Remaining charge: %u, Capacity: %u\n", _current, _battery_voltage, _remaining_charge, _capacity);
                    Serial.print(battery_info);
                }
                else
                {
                    char error_string[15];
                    sprintf(&error_string[0], "Error:%u\n", result);
                    Serial.print(error_string);
                }
                _comState = ComState::READ_TEMPS;
                break;
            }
        }   
    } 
}

float RenogyBattery::getSoc() const
{
    if(_capacity > 0)
    {
        return (_remaining_charge*100.0f)/static_cast<float>(_capacity);
    }
    else
    {
        return 0.0;
    }
    
}

float RenogyBattery::getRemainingCharge() const
{
    // Remaining charge is stored in mAh.
    return _remaining_charge*100.0f;
}

float RenogyBattery::getSoh() const
{
    if(_nominal_capacity > 0)
    {
        return (_capacity*100.0f)/static_cast<float>(_nominal_capacity);
    }
    else
    {
        return 0.0f;
    }
}

float RenogyBattery::getTemperature() const
{
    sint32_t sum = 0;

    sum += _cell_temps[0];
    sum += _cell_temps[1];
    sum += _cell_temps[2];
    sum += _cell_temps[3];
    // cell temps are stored in fixed point with 0.1°C resolution
    return (sum*0.1f)/4.0f;
}

float RenogyBattery::getVoltage() const
{
    // voltage is stored in fixed point with 0.1V resolution
    return _battery_voltage * 0.1f;
}

float RenogyBattery::getCurrent() const
{
    // current is stored in fixed point with 0.01 A resolution
    return _current * 0.01f;
}