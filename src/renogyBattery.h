#ifndef RENOGY_BATTERY_H
#define RENOGY_BATTERY_H

#include <ModbusMaster.h>
#include <cstdint>

typedef uint32_t charge_t;  //!< Charge in mAh.
typedef uint16_t voltage_t; //!< Voltage in 0.1 V resolution.
typedef sint16_t current_t; //!< Current in 0.01 A resolution.
typedef sint16_t temperature_t;   //!< Temperature in 0.1°C resolution.

#define RENOGY_BATTERY_TRANSMISSION_DELAY_MILLIS  50    // The battery needs a minimum seperation between transmissions.

class RenogyBattery
{
    public:
        RenogyBattery(float nominal_capacity_Ah, Stream& serial, uint16_t readout_cycletime);

        void begin(uint8_t slave_address = 247);
        void handle_communication();

        /**
         * Remaining charge in Ah.
         */
        float getRemainingCharge() const;

        /**
         * State of charge in percent.
         */
        float getSoc() const;

        /**
         * State of health in percent.
         */
        float getSoh() const;

        /**
         * Average cell temperature in degrees celcius.
         */
        float getTemperature() const;

        /**
         * Battery voltage.
         */
        float getVoltage() const;

        /**
         * Battery current.
         */
        float getCurrent() const;

    private:
        enum class ComState: uint8
        {
            READ_TEMPS = 0,
            READ_BATTERYINFO
        };
        const uint16_t _cycletime;
        const charge_t _nominal_capacity;
        Stream& _serial;
        uint8_t _slave_address;
        ModbusMaster _node;

        ComState _comState;
        long _lastTransmissionMillis;
        long _startOfReadoutCycleMillis;

        charge_t _remaining_charge;
        charge_t _capacity;
        voltage_t _battery_voltage;
        current_t _current;
        temperature_t _cell_temps[4];
};

#endif /* RENOGY_BATTERY_H */